DAILY SCRUM

SQL server name: easyrx.database.windows.net
Username: sqladmin
Password: Canada!1

2020/12/18 Maxime
1.
Created project
Created database mockup
2. 
Final database model
Import the classes to the project
3.
Research database first EF

Serge
1.
Created the intro for project
Designed the mockups for the main windows
2.
Help with the final database model
Finish setting project on my machine
3.
Find and test a library for PDF printouts

2020/12/23
Maxime
1.
Worked on final database model.
Put together the project proposal.
Researched login with C# and WPF.
2.
Finalize the database model and implement it.
Create a login window
3.
Research the best way to safely store passwords

Serge
1. Finalized Datatypes excel
2. Updated mockup
3. Studied for final exam

2020/12/28
Serge
1. Reviewed mockup, reviewed basic software interactions
2. Updated mockup
3. Created draft of main window look

2021/01/02
Serge
1. Reviewed mockup, updated/removed redundancies
2. Updated main window, added Dialogs/popups for several buttons

2021/01/11
Serge
1. Reviewed mockups/design (again)
2. Cut content
3. Implemented basic interactions

2021/01/12
Serge
1. Set goals for week
2. Finish implementing window interactions
3. Set placeholder scripts for CRUD
Maxime
1. Created tables
2. Import classes to project
3. Research password hashing

2021/01/13
Maxime
1. Fixed merge problems, created login screen
2. Create password hashing code
3. Furter research security best practices
Serge
1. Finish Main Window and Dialogs layout
2. Continue work on scripts

2021/01/14
Serge
1. Made modifications to windows
2. Continue scripts

2021/01/15
Serge
1. Finishing CRUD scripts, main goal is to complete that today.
2. Test scripts

2021/01/16
Serge
1. Tested CRUD scripts, broke things as much as possible
2. Worked on PowerPoint
3. Tested application with laymen
Maxime

2020/01/17
Maxime
1. Finished login and register classes
2. Finalize PDF creation and printing. Perform last sweep of the app
3. Finalized PowerPoint slides
Serge
1. Finalized CRUD scripts
2. Tested all implementations
3. Finalized PowerPoint slides

2020/01/18
PRESENTATION

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow
{
    /// <summary>
    /// Interaction logic for ClientMasterListDialog.xaml
    /// </summary>
    public partial class ClientMasterListDialog : Window
    {
        public Customer selectedCustomer { get; set; }

        public ClientMasterListDialog()
        {
            InitializeComponent();
            LoadAllCustomers();
        }

        private void LoadAllCustomers()
        {
            try
            {
                /*var customers =
                    (
                        from c in Globals.context.Customers
                        select c.Id
                    ).Distinct().ToList();*/
                List<Customer> list = Globals.context.Customers.ToList();
                lvCustomerMasterList_dlgClientMasterList.ItemsSource = list;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnSelectCustomer_dlgClientMasterList_Click(object sender, RoutedEventArgs e)
        {
            if (lvCustomerMasterList_dlgClientMasterList.SelectedIndex > -1)
            {
                this.selectedCustomer = (Customer)lvCustomerMasterList_dlgClientMasterList.SelectedItem;

                this.DialogResult = true;
            }  
        }

        private void btnDeleteCustomer_dlgClientMasterList_Click(object sender, RoutedEventArgs e)
        {
            if (lvCustomerMasterList_dlgClientMasterList.SelectedIndex > -1)
            {
                Customer customer = (Customer)lvCustomerMasterList_dlgClientMasterList.SelectedItem;
                
                Globals.context.Customers.Remove(customer);
                Globals.context.SaveChanges();
                LoadAllCustomers();
            }
        }

        private void btnCancel_dlgClientMasterList_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ctxDelete_dlgClientMasterList_Click(object sender, RoutedEventArgs e)
        {
            if (lvCustomerMasterList_dlgClientMasterList.SelectedIndex > -1)
            {
                Customer customer = (Customer)lvCustomerMasterList_dlgClientMasterList.SelectedItem;
                
                Globals.context.Customers.Remove(customer);
                Globals.context.SaveChanges();
                LoadAllCustomers();
            }
        }

        private void lvCustomerMasterList_dlgClientMasterList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCustomerMasterList_dlgClientMasterList.SelectedIndex > -1)
            {
                btnDeleteCustomer_dlgClientMasterList.IsEnabled = true;
            }
            if (lvCustomerMasterList_dlgClientMasterList.SelectedIndex == -1)
            {
                btnDeleteCustomer_dlgClientMasterList.IsEnabled = false;
            }
        }

        private void lvCustomerMasterList_dlgClientMasterList_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            if ( lvCustomerMasterList_dlgClientMasterList.SelectedIndex == -1 ) { return; }

            this.selectedCustomer = (Customer)lvCustomerMasterList_dlgClientMasterList.SelectedItem;

            this.DialogResult = true;
        }

        private void btSearch_Click(object sender, RoutedEventArgs e) {
            string searchString = tbSearchBox.Text;

            List<Customer> searchResult = (from c in Globals.context.Customers
                                           where c.MainPhone.Contains(searchString) ||
                                           c.LastName.Contains(searchString)
                                           select c).ToList();
            
            if(searchResult.Count > 0) {
                lvCustomerMasterList_dlgClientMasterList.ItemsSource = searchResult;
            } else {
                MessageBox.Show("No customer found.", "Search Result", MessageBoxButton.OK, MessageBoxImage.Warning);
                LoadAllCustomers();
            }
        }
    }
}

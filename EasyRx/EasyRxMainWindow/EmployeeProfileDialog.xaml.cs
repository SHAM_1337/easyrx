﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow
{
    /// <summary>
    /// Interaction logic for EmployeeProfileDialog.xaml
    /// </summary>
    public partial class EmployeeProfileDialog : Window
    {

        Employee currUser;
        public EmployeeProfileDialog(Employee employee)
        {
            InitializeComponent();
            currUser = employee;
            PopulateFields();
        }

        private void btnSaveProfile_EmployeeProfileDialog_Click(object sender, RoutedEventArgs e) {
            if (!ValidateForm()) { return; }

            string[] name = tbName_EmployeeProfileDialog.Text.Split(' ');
            string fName = name[0];
            string lName = name[1];

            currUser.FirstName = fName;
            currUser.LastName = lName;
            currUser.DateOfBirth = (DateTime)dpDatePicker.SelectedDate;
            currUser.Address = tbAddress_EmployeeProfileDialog.Text;
            currUser.City = tbCity_EmployeeProfileDialog.Text;
            currUser.PostalCode = tbPostalCode_EmployeeProfileDialog.Text;
            currUser.Province = comboProvince_EmployeeProfileDialog.Text;
            currUser.MainPhone = tbMainPhone_EmployeeProfileDialog.Text;
            currUser.SecondaryPhone = tbSecondPhone_EmployeeProfileDialog.Text;
            currUser.Email = tbEmail_EmployeeProfileDialog.Text;
            currUser.Title = cbTitle.Text;

            if (tbOldPassword.Password.Length < 0) {
                string hash = Utils.GenerateHash(tbNewPassword.Password, currUser.PasswordSalt);
                currUser.PasswordHash = hash;
            }

            Globals.context.SaveChanges();
            DialogResult = true;
        }

        private bool ValidateForm() {

            List<string> errorsList = new List<string>();
            Regex textRg = new Regex(@"[A-Za-z]{1,50}");
            Regex phoneRg = new Regex(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$");
            var emailValidator = new EmailAddressAttribute();
            Regex addressRg = new Regex(@"\d{1,5}\s\w\w");
            Regex postalCodeRg = new Regex(@"^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$");
            Regex passwordRg = new Regex(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$");

            string[] name = tbName_EmployeeProfileDialog.Text.Split(' ');
            string fname = name[0];
            string lname = name[1];

            if (!textRg.IsMatch(fname)) {
                errorsList.Add("First name must be 1 to 50 characters and contain only letters.");
            }
            if (!textRg.IsMatch(lname)) {
                errorsList.Add("Last name must be 1 to 50 characters and contain only letters.");
            }
            if (dpDatePicker.SelectedDate == null) {
                errorsList.Add("A date of birth must be selected.");
            }
            if (!phoneRg.IsMatch(tbMainPhone_EmployeeProfileDialog.Text)) {
                errorsList.Add("Phone number must contain 10 numbers.");
            }
            if (tbSecondPhone_EmployeeProfileDialog.Text.Length < 0) {
                if (!phoneRg.IsMatch(tbSecondPhone_EmployeeProfileDialog.Text)) {
                    errorsList.Add("Phone number must contain 10 numbers.");
                }
            }
            if (tbEmail_EmployeeProfileDialog.Text.Length < 0) {
                if (!emailValidator.IsValid(tbEmail_EmployeeProfileDialog.Text) || tbEmail_EmployeeProfileDialog.Text.Length > 100) {
                    errorsList.Add("Email must be formatted as john@doe.com and be less than 100 characters.");
                }
            }
            if (!addressRg.IsMatch(tbAddress_EmployeeProfileDialog.Text) || tbAddress_EmployeeProfileDialog.Text.Length > 100) {
                errorsList.Add("A valid address must be entered.");
            }
            if (!textRg.IsMatch(tbCity_EmployeeProfileDialog.Text)) {
                errorsList.Add("City name must be 1 to 50 characters and contain only letters.");
            }
            if (!postalCodeRg.IsMatch(tbPostalCode_EmployeeProfileDialog.Text)) {
                errorsList.Add("Postal code must be formatted as A1A1A1 or A1A 1A1.");
            }
            if (comboProvince_EmployeeProfileDialog.SelectedIndex == -1) {
                errorsList.Add("A province must be selected.");
            }
            if (cbTitle.SelectedIndex == -1) {
                errorsList.Add("A title must be selected.");
            }

            if (tbOldPassword.Password.Length > 0) {
                if (!passwordRg.IsMatch(tbNewPassword.Password)) {
                    errorsList.Add("Password must be at least 8 characters and contain one letter and one number.");
                }
            }
            if (tbNewPassword.Password != tbConfirmPassword.Password) {
                errorsList.Add("Password must be correctly confirmed.");
            }
            if (errorsList.Count != 0) {
                MessageBox.Show(string.Join("\n", errorsList), "Registration Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return (errorsList.Count == 0);
        }

        private void PopulateFields() {
            tbName_EmployeeProfileDialog.Text = currUser.FirstName + " " + currUser.LastName;
            dpDatePicker.SelectedDate = currUser.DateOfBirth;
            tbAddress_EmployeeProfileDialog.Text = currUser.Address;
            tbCity_EmployeeProfileDialog.Text = currUser.City;
            tbMainPhone_EmployeeProfileDialog.Text = currUser.MainPhone;
            tbSecondPhone_EmployeeProfileDialog.Text = currUser.SecondaryPhone;
            tbEmail_EmployeeProfileDialog.Text = currUser.Email;
            cbTitle.SelectedItem = cbTitle.Items.OfType<ComboBoxItem>().FirstOrDefault(x => x.Content.ToString() == currUser.Title);
            tbPostalCode_EmployeeProfileDialog.Text = currUser.PostalCode;
            comboProvince_EmployeeProfileDialog.SelectedItem = comboProvince_EmployeeProfileDialog.Items.OfType<ComboBoxItem>().FirstOrDefault(x => x.Content.ToString() == currUser.Province);
        }
    }
}

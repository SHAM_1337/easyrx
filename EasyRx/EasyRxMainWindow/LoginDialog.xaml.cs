﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow {
    /// <summary>
    /// Interaction logic for LoginDialog.xaml
    /// </summary>
    public partial class LoginDialog : Window {

        public event Action<Employee> ReturnEmployee;

        public LoginDialog() {
            InitializeComponent();
        }

        private void btRegister_Click(object sender, RoutedEventArgs e) {
            RegisterDialog registerDlg = new RegisterDialog();
            registerDlg.ShowDialog();
        }

        private void btLogin_Click(object sender, RoutedEventArgs e) {

            // Search employee by username
            string username = tbUsername.Text;
            var employee = (from emp in Globals.context.Employees
                           where emp.Username == username
                           select emp).FirstOrDefault();

            if (employee == null) {
                MessageBox.Show("Please enter a valid username/password", "Invalid username/password", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            // Retrieving password hash and salt
            string enteredPassword = tbPassword.Password;
            string passwordHash = employee.PasswordHash;
            string salt = employee.PasswordSalt;


            if (Utils.CompareHash(enteredPassword, passwordHash, salt)) {
                ReturnEmployee?.Invoke(employee);
                DialogResult = true;
            } else {
                MessageBox.Show("Please enter a valid username/password", "Invalid username/password", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btCloseProgram_Click(object sender, RoutedEventArgs e) {
            Environment.Exit(1);
        }

    }
}

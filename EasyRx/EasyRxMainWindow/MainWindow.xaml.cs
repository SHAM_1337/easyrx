﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyRxMainWindow {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        Employee currentUser = null;
        Customer currentCustomer = null;
        List<Prescription> currentCustomerRx;
        public MainWindow() {
            InitializeComponent();
            try {
                Globals.context = new EasyRxConnection();
                //MessageBox.Show("Connected to DB", "Connection completed", MessageBoxButton.OK, MessageBoxImage.Information);
                LoadInsurancePlans();
                // GetDBRecordsMainWindow();
            } catch (SystemException ex) {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal connection error
            }
            GenerateLogin();
            PermissionHandler();
        }

        public void LoadInsurancePlans() {
            try {
                var insurancePlans =
                    (
                        from c in Globals.context.InsurancePlans
                        select c.InsuranceCompany
                    ).Distinct().ToList();

                insurancePlans.Add("None");
                comboInsurancePlan.ItemsSource = insurancePlans;
                comboInsurancePlan.SelectedItem = "None";
            } catch (SystemException ex) {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LoadAllRx() {
            try {
                // Select for the list view
                var RxList =
                    (
                        from r in Globals.context.Prescriptions
                        join d in Globals.context.Doctors on r.DoctorId equals d.Id
                        join f in Globals.context.Drugs on r.DrugId equals f.Id
                        where r.CustomerId == currentCustomer.Id
                        select new { DrugName = f.Name, r.Dosage, r.Posology, r.NumberOfRefills, r.Date, DoctorName = d.FirstName + " " + d.LastName }
                    ).ToList();

                // Select for the object to modify
                currentCustomerRx = (from p in Globals.context.Prescriptions
                                     where p.CustomerId == currentCustomer.Id
                                     select p).ToList();


                lvRxChart.ItemsSource = RxList;
            } catch (SystemException ex) {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnClientMasterList_Click(object sender, RoutedEventArgs e) {
            ClientMasterListDialog clientMasterListdlg = new ClientMasterListDialog();
            clientMasterListdlg.Owner = this;
            if (clientMasterListdlg.ShowDialog() == true) {
                currentCustomer = clientMasterListdlg.selectedCustomer;
                lblCustomerName.Content = currentCustomer.FirstName + " " + currentCustomer.LastName;
                LoadSelectedCustomer();
                LoadAllRx();
                UpdateBtn();
            }
        }

        private void LoadSelectedCustomer() {
            tbName.Text = currentCustomer.FirstName + " " + currentCustomer.LastName;
            dpDateofBirth.SelectedDate = currentCustomer.DateOfBirth;
            tbAddress.Text = currentCustomer.Address;
            tbCity.Text = currentCustomer.City;
            tbMainPhone.Text = currentCustomer.MainPhone;
            tbSecondPhone.Text = currentCustomer.SecondaryPhone;
            tbEmail.Text = currentCustomer.Email;
            tbHeight.Text = currentCustomer.Height.ToString();
            tbWeight.Text = currentCustomer.Weight.ToString();
            tbPostalCode.Text = currentCustomer.PostalCode;
            comboProvince.Text = currentCustomer.Province;
            tbCustomerNotes.Text = currentCustomer.Note;

            if (currentCustomer.InsurancePlanId != null) {
                comboInsurancePlan.Text = currentCustomer.InsurancePlan.InsuranceCompany;
            } else {
                comboInsurancePlan.SelectedItem = "None";
            }

            if ((currentCustomer.Gender).Equals("M")) {
                rbMaleGender.IsChecked = true;
            }
            if ((currentCustomer.Gender).Equals("F")) {
                rbFemaleGender.IsChecked = true;
            }
        }

        private void btnPrescribe_Click(object sender, RoutedEventArgs e) {
            PrescriptionAssignmentDialog prescribedlg = new PrescriptionAssignmentDialog(currentCustomer);
            prescribedlg.Owner = this;
            if (prescribedlg.ShowDialog() == true) {
                LoadAllRx();
            }
        }

        private void btnMyProfile_Click(object sender, RoutedEventArgs e) {
            EmployeeProfileDialog employeeProfiledlg = new EmployeeProfileDialog(currentUser);
            employeeProfiledlg.Owner = this;
            employeeProfiledlg.ShowDialog();
            UpdateStatusText();
        }
        private void btnLogout_Click(object sender, RoutedEventArgs e) {
            ClearFields();
            tiCustomerView.IsSelected = true;
            currentUser = null;
            tbGreeting.Text = "";
            currentCustomerRx = null;
            GenerateLogin();
        }

        private void btnSaveCustomerNotes_Click(object sender, RoutedEventArgs e) {
            currentCustomer.Note = tbCustomerNotes.Text;
            Globals.context.SaveChanges();
            MessageBox.Show("Customer Notes Updated.", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnClearFields_Click(object sender, RoutedEventArgs e) {
            ClearFields();
        }

        private void btnCreateUpdateCustomerProfile_Click(object sender, RoutedEventArgs e) {
            if (currentCustomer != null) // if customer exists, update
            {
                if (ValidateForm() == true) {
                    try {
                        string[] name = tbName.Text.Split(' ');
                        string fName = name[0];
                        string lName = name[1];

                        string gender = "";
                        if (rbMaleGender.IsChecked == true) {
                            gender = "M";
                        } else {
                            gender = "F";
                        }

                        currentCustomer.FirstName = fName;
                        currentCustomer.LastName = lName;
                        currentCustomer.DateOfBirth = (DateTime)dpDateofBirth.SelectedDate;
                        currentCustomer.Gender = gender;
                        currentCustomer.Address = tbAddress.Text;
                        currentCustomer.City = tbCity.Text;
                        currentCustomer.PostalCode = tbPostalCode.Text;
                        currentCustomer.Province = comboProvince.Text;
                        currentCustomer.MainPhone = tbMainPhone.Text;
                        currentCustomer.SecondaryPhone = tbSecondPhone.Text;
                        currentCustomer.Email = tbEmail.Text;
                        if (comboInsurancePlan.Text == "None") {
                            currentCustomer.InsurancePlanId = null;
                        } else {
                            var id = (from i in Globals.context.InsurancePlans
                                      where i.InsuranceCompany == comboInsurancePlan.Text
                                      select i.Id).FirstOrDefault();

                            currentCustomer.InsurancePlanId = id;
                        }

                        //Globals.context.Update(currentCustomer);
                        Globals.context.SaveChanges();

                        MessageBox.Show("Customer Profile Updated.", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
                    } catch (SystemException ex) {
                        MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }

            if (currentCustomer == null) // if customer doesn't exist, create
            {
                if (ValidateForm() == true) {
                    try {
                        string[] name = tbName.Text.Split(' ');
                        string fName = name[0];
                        string lName = name[1];

                        System.DateTime DoB = (DateTime)dpDateofBirth.SelectedDate;

                        string gender = "";
                        if (rbMaleGender.IsChecked == true) {
                            gender = "M";
                        } else {
                            gender = "F";
                        }

                        string mainPhone = tbMainPhone.Text;

                        string secondaryPhone = tbSecondPhone.Text;

                        string email = tbEmail.Text;
                        string address = tbAddress.Text;
                        string city = tbCity.Text;
                        string postalCode = tbPostalCode.Text;
                        string province = comboProvince.Text;
                        double height = Convert.ToDouble(tbHeight.Text); // exceptions weeded out by Regex
                        double weight = Convert.ToDouble(tbWeight.Text);

                        // Changes here
                        string insuranceName = comboInsurancePlan.Text;

                        int? insuranceId = null;
                        if (comboInsurancePlan.Text != "None") { 
                       
                            var id = (from i in Globals.context.InsurancePlans
                                      where i.InsuranceCompany == comboInsurancePlan.Text
                                      select i.Id).FirstOrDefault();

                        }

                        Customer customer = new Customer {
                                FirstName = fName,
                                LastName = lName,
                                DateOfBirth = DoB,
                                Gender = gender,
                                MainPhone = mainPhone,
                                SecondaryPhone = secondaryPhone,
                                Email = email,
                                Address = address,
                                City = city,
                                PostalCode = postalCode,
                                Province = province,
                                Height = height,
                                Weight = weight,
                                InsurancePlanId = insuranceId
                            };

                        Globals.context.Customers.Add(customer);
                        Globals.context.SaveChanges();
                        MessageBox.Show("Customer Profile Created.", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
                        ClearFields();
                    } catch (SystemException ex) {
                        MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }

        }

        private bool ValidateForm() {
            List<string> errorsList = new List<string>();
            Regex textRg = new Regex(@"[A-Za-z\-\']{1,100}");
            Regex phoneRg = new Regex(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$");
            Regex emailRg = new Regex(@"^[\w -\.] +@([\w -] +\.) +[\w -]{ 2,4}$");
            Regex addressRg = new Regex(@"\d{1,5}\s\w\w");
            Regex postalCodeRg = new Regex(@"^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$");
            Regex heightWeightRg = new Regex(@"^[0-9]{1,4}[\.\,]{0,1}[0-9]{0,3}$");

            if (!heightWeightRg.IsMatch(tbHeight.Text)) {
                errorsList.Add("Please enter your height in cm, format \"####.###\" commas or periods allowed.");
            }
            if (!heightWeightRg.IsMatch(tbWeight.Text)) {
                errorsList.Add("Please enter your weight in kg, format \"####.###\" commas or periods allowed.");
            }
            if (!textRg.IsMatch(tbName.Text)) {
                errorsList.Add("Name must be 1 to 100 characters and contain only letters, hyphens or apostrophes.");
            }
            if (dpDateofBirth.SelectedDate == null) {
                errorsList.Add("A date of birth must be selected.");
            }
            if (rbMaleGender.IsChecked == false && rbFemaleGender.IsChecked == false) {
                errorsList.Add("Please select a gender.");
            }
            if (!phoneRg.IsMatch(tbMainPhone.Text)) {
                errorsList.Add("Phone number must contain 10 numbers.");
            }
            if (tbSecondPhone.Text.Length < 0) {
                if (!phoneRg.IsMatch(tbSecondPhone.Text)) {
                    errorsList.Add("Secondary phone number must contain 10 numbers.");
                }
            }
            if (tbEmail.Text.Length < 0) {
                if (!emailRg.IsMatch(tbEmail.Text) || tbEmail.Text.Length > 100) {
                    errorsList.Add("Email must be formatted as john@doe.com and be less than 100 characters.");
                }
            }
            if (!addressRg.IsMatch(tbAddress.Text) || tbAddress.Text.Length > 100) {
                errorsList.Add("A valid address must be entered.");
            }
            if (!textRg.IsMatch(tbCity.Text)) {
                errorsList.Add("City name must be 1 to 50 characters and contain only letters.");
            }
            if (!postalCodeRg.IsMatch(tbPostalCode.Text)) {
                errorsList.Add("Postal code must be formatted as A1A1A1 or A1A 1A1.");
            }
            if (comboProvince.SelectedIndex == -1) {
                errorsList.Add("A province must be selected.");
            }
            if (errorsList.Count != 0) {
                MessageBox.Show(string.Join("\n", errorsList), "Customer Entry Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }
        private void GenerateLogin() {
            while (currentUser == null) {
                LoginDialog loginDlg = new LoginDialog();
                loginDlg.ReturnEmployee += value => currentUser = value;
                loginDlg.ShowDialog();
            }
            UpdateStatusText();
        }

        private void PermissionHandler() {
            switch (currentUser.Title) {
                case "Pharmacy Tech.":
                    btnPrescribe.IsEnabled = false;
                    
                    break;
                case "Cashier":
                    tiRxCharts.IsEnabled = false;
                    tiNotes.IsEnabled = false;
                    btnPrescribe.IsEnabled = false;
                    btRefillRx.IsEnabled = false;
                    break;
                default:
                    break;
            }
        }

        private void UpdateStatusText() {
            tbGreeting.Text = String.Format("Hello {0} {1} and welcome to EasyRx!", currentUser.FirstName, currentUser.LastName);
        }

        private void UpdateBtn() {
            btnCreateUpdateCustomerProfile.Content = "Update Customer Profile";
            btnPrescribe.IsEnabled = true;
            tiRxCharts.IsEnabled = true;
            tiNotes.IsEnabled = true;
            btExportPdf.IsEnabled = true;
            btPrint.IsEnabled = true;
            PermissionHandler();
        }
        private void RevertBtn() {
            btnCreateUpdateCustomerProfile.Content = "Create New Customer Profile";
            btnPrescribe.IsEnabled = false;
            tiRxCharts.IsEnabled = false;
            tiNotes.IsEnabled = false;
            btExportPdf.IsEnabled = false;
            btPrint.IsEnabled = false;
            PermissionHandler();
        }

        private void ClearFields() {
            string empty = "";
            currentCustomer = null;
            lblCustomerName.Content = "No Customer Selected";
            tbName.Text = empty;
            dpDateofBirth.SelectedDate = null;
            tbAddress.Text = empty;
            tbCity.Text = empty;
            tbMainPhone.Text = empty;
            tbSecondPhone.Text = empty;
            tbEmail.Text = empty;
            rbMaleGender.IsChecked = false;
            rbFemaleGender.IsChecked = false;
            tbHeight.Text = empty;
            tbWeight.Text = empty;
            tbPostalCode.Text = empty;
            comboProvince.SelectedItem = comboDefault;
            comboInsurancePlan.SelectedItem = "None";
            RevertBtn();
        }

        private void btExportPdf_Click(object sender, RoutedEventArgs e) {
            SaveFileDialog exportDialog = new SaveFileDialog();
            exportDialog.Filter = "PDF file (*.pdf)|*.pdf";
            exportDialog.Title = "Export to PDF";

            if (exportDialog.ShowDialog() == true) {
                string exportPath = exportDialog.FileName;
                PDFCreator.CreatePDF(currentCustomer, exportPath);
            }
        }

        private void btPrint_Click(object sender, RoutedEventArgs e) {
            PrintHelper.PrintPdf(currentCustomer);
        }

        private void btRefillRx_Click(object sender, RoutedEventArgs e) {
            var prescription = currentCustomerRx[lvRxChart.SelectedIndex];
            
            if (prescription.NumberOfRefills > 0) {
                prescription.NumberOfRefills -= 1;

                Refill refill = new Refill { Date = DateTime.Now, EmployeeId = currentUser.Id, PrescriptionId = prescription.Id };
                Globals.context.Refills.Add(refill);
                Globals.context.SaveChanges();
                MessageBox.Show("Refill successful", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
                LoadAllRx();
            }
        }

        private void lvRxChart_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (lvRxChart.SelectedIndex == -1) { return; }

            btRefillRx.IsEnabled = true;
            PermissionHandler();
        }
    }
}

﻿using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyRxMainWindow {
    class PDFCreator {
        public static void CreatePDF(Customer customer, string path) {
            PdfWriter writer = new PdfWriter(path);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            Paragraph header = new Paragraph("EasyRx")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(20);

            document.Add(header);

            LineSeparator ls = new LineSeparator(new SolidLine());
            document.Add(ls);

            Paragraph custInfo = new Paragraph(
                string.Format("{0} {1} \n{2} \n{3} \n {4}, {5}, {6}, {7} ", customer.FirstName, customer.LastName, customer.DateOfBirth, customer.MainPhone, customer.Address, customer.City, customer.PostalCode, customer.Province));
            document.Add(custInfo);

            List<Prescription> custPrescriptions = (from p in Globals.context.Prescriptions
                                                   where p.CustomerId == customer.Id
                                                   select p).ToList();

            Paragraph prescriptionTitle = new Paragraph("Prescriptions");
            document.Add(prescriptionTitle);
            document.Add(ls);
            foreach(Prescription p in custPrescriptions) {
                Paragraph prescription = new Paragraph(string.Format(
                    "{0}\n" +
                    "Dosage: {1}\n" +
                    "Posology: {2}\n" +
                    "Refills: {3}" +
                    "Prescriptor: {4} {5}\n" +
                    "Date: {6}", p.Drug.Name, p.Dosage, p.Posology, p.NumberOfRefills, p.Doctor.FirstName, p.Doctor.LastName, p.Date));
                document.Add(prescription);
            }

            document.Close();
        }
    }
}

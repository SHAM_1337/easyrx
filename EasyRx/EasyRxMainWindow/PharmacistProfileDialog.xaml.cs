﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow
{
    /// <summary>
    /// Interaction logic for PharmacistProfileDialog.xaml
    /// </summary>
    public partial class PharmacistProfileDialog : Window
    {
        Clinic selectedClinic = null;
        // Doctor enteredPharmacist = null;
        public Doctor selectedPharmacist { get; set; }
        public PharmacistProfileDialog()
        {
            InitializeComponent();
            LoadClinics();
        }

        private void LoadClinics()
        {
            try
            {
                List<Clinic> list = Globals.context.Clinics.ToList();
                lvClinics_PharmacistProfileDialog.ItemsSource = list;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnSaveProfile_PharmacistProfileDialog_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateForm() == true)
            {
                string[] name = tbName_PharmacistProfileDialog.Text.Split(' ');
                string fName = name[0];
                string lName = name[1];
                string practiceNumber = tbPracticeNumber_PharmacistProfileDialog.Text;

                selectedClinic = (Clinic)(lvClinics_PharmacistProfileDialog.SelectedItem);
                int clinicId = selectedClinic.Id;

                Doctor doctor = new Doctor
                {
                    FirstName = fName,
                    LastName = lName,
                    PracticeNumber = practiceNumber,
                    ClinicId = clinicId
                };

                Globals.context.Doctors.Add(doctor);
                Globals.context.SaveChanges();
                MessageBox.Show("Pharmacist Profile Updated.", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);
                this.selectedPharmacist = doctor;
                this.DialogResult = true;
            }

        }
        private bool ValidateForm()
        {
            
            List<string> errorsList = new List<string>();
            Regex textRg = new Regex(@"[A-Za-z\-\']{1,100}");
            Regex practiceRg = new Regex(@"^[A-Z]{4}[\-][0-9]{4}[\-][0-9]{4}$");

            if (!textRg.IsMatch(tbName_PharmacistProfileDialog.Text))
            {
                errorsList.Add("Name must be 1 to 100 characters and contain only letters, hyphens or apostrophes.");
                return false;
            }
            if (!textRg.IsMatch(tbPracticeNumber_PharmacistProfileDialog.Text))
            {
                errorsList.Add("Please enter a Practice Number / MINC in the format CAMD-1234-5679");
                return false;
            }
            if (errorsList.Count != 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Pharmacist Entry Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (lvClinics_PharmacistProfileDialog.SelectedIndex > -1)
            {
                this.selectedClinic = (Clinic)lvClinics_PharmacistProfileDialog.SelectedItem;
                return true;
            }
            return false;
        }
    }
}

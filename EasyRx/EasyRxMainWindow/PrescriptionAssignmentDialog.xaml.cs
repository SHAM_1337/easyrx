﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow
{
    /// <summary>
    /// Interaction logic for PrescriptionAssignmentDialog.xaml
    /// </summary>
    public partial class PrescriptionAssignmentDialog : Window
    {
        Doctor currentPharmacist = null;
        Customer currentCustomer = null;
        public PrescriptionAssignmentDialog(Customer customer)
        {
            currentCustomer = customer;
            InitializeComponent();
            LoadDrugs();
            LoadCustomer();
            LoadPharmacists();
        }

        public void LoadCustomer()
        {
            lblCustomerName_dlgPrescriptionAssignment.Content = currentCustomer.FirstName + " " + currentCustomer.LastName;
        }

        public void LoadDrugs()
        {
            try
            {
                var Drugs =
                    (
                        from d in Globals.context.Drugs
                        select d.Name
                    ).Distinct().ToList();
                comboRxName_dlgPrescriptionAssignment.ItemsSource = Drugs;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void LoadPharmacists()
        {
            try
            {
                var Pharmacists =
                    (
                        from p in Globals.context.Doctors
                        select p.FirstName + " " + p.LastName
                    ).Distinct().ToList();
                comboChoosePharmacist_dlgPrescriptionAssignment.ItemsSource = Pharmacists;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void comboChoosePharmacist_dlgPrescriptionAssignment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string[] pharmacistName = comboChoosePharmacist_dlgPrescriptionAssignment.Text.Split(' ');
            string pharmacistFName = pharmacistName[0];
            
            Doctor currentPharmacist = 
                                (
                                    from p in Globals.context.Doctors
                                    where p.FirstName == pharmacistFName
                                    select p
                                ).FirstOrDefault();
        }

        private void comboRxName_dlgPrescriptionAssignment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Drug drug =
                    (
                        from d in Globals.context.Drugs
                        where d.Name == comboRxName_dlgPrescriptionAssignment.SelectedItem.ToString()
                        select d
                    ).FirstOrDefault();

            lblProvider_dlgPrescriptionAssignment.Content = drug.Provider;
            lblCost_dlgPrescriptionAssignment.Content = drug.Cost;
            lblPrice_dlgPrescriptionAssignment.Content = drug.Price;

            if (drug.IsGeneric == true)
            {
                lblGeneric_dlgPrescriptionAssignment.Content = "Yes";
            }
            if (drug.IsGeneric == false)
            {
                lblGeneric_dlgPrescriptionAssignment.Content = "No";
            }
        }
        private void btnEnterNewPharmacist_dlgPrescriptionAssignment_Click(object sender, RoutedEventArgs e)
        {
            PharmacistProfileDialog pharmacistProfileDialog = new PharmacistProfileDialog();
            pharmacistProfileDialog.Owner = this;

            if (pharmacistProfileDialog.ShowDialog() == true)
            {
                LoadPharmacists();
                currentPharmacist = pharmacistProfileDialog.selectedPharmacist;
                string pharmacistName = currentPharmacist.FirstName + " " + currentPharmacist.LastName;
                comboChoosePharmacist_dlgPrescriptionAssignment.SelectedValue = pharmacistName;
            }
            LoadPharmacists();
        }
        private bool ValidateForm()
        {

            List<string> errorsList = new List<string>();
            Regex textRg = new Regex(@"^[0-9A-Za-z\-\'\,\@\+\#\* ]{1,50}$");
            Regex numberRg = new Regex(@"^[0-9]{1,2}$");
            Regex posologyRg = new Regex(@"^[0-9A-Za-z\-\'\,\@\+\#\*\. ]{1,100}$");

            if (comboChoosePharmacist_dlgPrescriptionAssignment.SelectedItem == null)
            {
                errorsList.Add("Please choose a prescribing Pharmacist");
            }

            if (!textRg.IsMatch(tbDosage_dlgPrescriptionAssignment.Text))
            {
                errorsList.Add("Please enter the dosage, 1-50 chars, most special characters allowed.");
            }
            if (!numberRg.IsMatch(tbRefills_dlgPrescriptionAssignment.Text))
            {
                errorsList.Add("Please enter the number of refills, only numbers 0-99 allowed.");
            }
            if (dpStartDate_dlgPrescriptionAssignment.SelectedDate == null)
            {
                errorsList.Add("A start date must be selected.");
            }
            if (!posologyRg.IsMatch(tbPosology_dlgPrescriptionAssignment.Text))
            {
                errorsList.Add("Please enter the posology, 1-50 chars, most special characters allowed.");
            }
            if (errorsList.Count != 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Prescription Entry Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }
        private void btnConfirmPrescription_dlgPrescriptionAssignment_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateForm() == true)
            {
                try
                {
                    string dosage = tbDosage_dlgPrescriptionAssignment.Text;
                    int refills = Convert.ToInt32(tbRefills_dlgPrescriptionAssignment.Text); // ex caught by Regex
                    System.DateTime startDate = (DateTime)dpStartDate_dlgPrescriptionAssignment.SelectedDate;
                    string posology = tbPosology_dlgPrescriptionAssignment.Text;
                    
                    string rxName = comboRxName_dlgPrescriptionAssignment.Text;
                    var drug =
                    (
                        from d in Globals.context.Drugs
                        where d.Name == rxName
                        select d
                    ).FirstOrDefault();

                    int drugId = drug.Id;

                    string[] pharmacistName = comboChoosePharmacist_dlgPrescriptionAssignment.Text.Split(' ');
                    string pharmacistFName = pharmacistName[0];

                    Doctor currentPharmacist =
                                        (
                                            from p in Globals.context.Doctors
                                            where p.FirstName == pharmacistFName
                                            select p
                                        ).FirstOrDefault();

                    Prescription Rx = new Prescription
                    {
                        Dosage = dosage,
                        Posology = posology,
                        NumberOfRefills = refills,
                        Date = startDate,
                        DoctorId = currentPharmacist.Id,
                        CustomerId = currentCustomer.Id,
                        DrugId = drugId
                    };

                    Globals.context.Prescriptions.Add(Rx);
                    Globals.context.SaveChanges();
                    MessageBox.Show("Prescription Updated.", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Information);

                    DialogResult = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}

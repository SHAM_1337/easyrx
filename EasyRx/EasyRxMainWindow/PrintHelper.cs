﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyRxMainWindow {
    class PrintHelper {
        public static void PrintPdf(Customer customer) {
            string path = Directory.GetCurrentDirectory() + "\\Test.pdf";
            PdfDocument pdfDoc = new PdfDocument();
            PDFCreator.CreatePDF(customer, path);
            pdfDoc.LoadFromFile(path);
            pdfDoc.PrintSettings.Copies = 1;
            pdfDoc.Print();
            pdfDoc.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasyRxMainWindow {
    /// <summary>
    /// Interaction logic for RegisterDialog.xaml
    /// </summary>
    public partial class RegisterDialog : Window {

        
        
        public RegisterDialog() {
            InitializeComponent();
        }

        private void btRegister_Click(object sender, RoutedEventArgs e) {
            if (!ValidateForm()) { return; }

            try {
                string fName = tbFName.Text;
                string lName = tbLName.Text;
                System.DateTime DoB = (DateTime)dpDoB.SelectedDate;

                // Only getting the first letter for database
                string gender = cbGender.Text.Substring(0, 1);
                string mainPhone = new string(tbMainPhone.Text.Where(char.IsDigit).ToArray()); //Removes any formatting
                string secondaryPhone = new string(tbSecondaryPhone.Text.Where(char.IsDigit).ToArray());
                string email = tbEmail.Text;
                string address = tbAddress.Text;
                string city = tbCity.Text;
                string postalCode = tbPostalCode.Text;
                string province = cbProvince.Text;
                string title = cbTitle.Text;
                string username = GenerateUsername();

                // Generating salt and hash for password storage
                string salt = Utils.GenerateSalt();
                string password = tbPassword.Password;
                string hash = Utils.GenerateHash(password, salt);

                Employee employee = new Employee { FirstName = fName, LastName = lName, DateOfBirth = DoB, Gender = gender, MainPhone = mainPhone, SecondaryPhone = secondaryPhone, Email = email, Address = address, City = city, PostalCode = postalCode, Province = province, Title = title, Username = username, PasswordSalt = salt, PasswordHash = hash };
                Globals.context.Employees.Add(employee);
                Globals.context.SaveChanges();
                MessageBox.Show("Registration completed. Your username is: " + username, "Username", MessageBoxButton.OK);
                DialogResult = true;
            } catch(SystemException ex) {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btReset_Click(object sender, RoutedEventArgs e) {
            ClearFields();
        }

        private bool ValidateForm() {
            List<string> errorsList = new List<string>();
            Regex textRg = new Regex(@"[A-Za-z]{1,50}");
            Regex phoneRg = new Regex(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$");
            var emailValidator = new EmailAddressAttribute();
            Regex addressRg = new Regex(@"\d{1,5}\s\w\w");
            Regex postalCodeRg = new Regex(@"^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$");
            Regex passwordRg = new Regex(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$");

            if (!textRg.IsMatch(tbFName.Text)) {
                errorsList.Add("First name must be 1 to 50 characters and contain only letters.");
            }
            if (!textRg.IsMatch(tbLName.Text)) {
                errorsList.Add("Last name must be 1 to 50 characters and contain only letters.");
            }
            if (dpDoB.SelectedDate == null) {
                errorsList.Add("A date of birth must be selected.");
            }
            if (cbGender.SelectedIndex == -1) {
                errorsList.Add("A gender must be selected");
            }
            if (!phoneRg.IsMatch(tbMainPhone.Text)) {
                errorsList.Add("Phone number must contain 10 numbers.");
            }
            if (tbSecondaryPhone.Text.Length < 0) {
                if (!phoneRg.IsMatch(tbSecondaryPhone.Text)) {
                    errorsList.Add("Phone number must contain 10 numbers.");
                }
            }
            if (tbEmail.Text.Length < 0) {
                if (!emailValidator.IsValid(tbEmail.Text) || tbEmail.Text.Length > 100) {
                    errorsList.Add("Email must be formatted as john@doe.com and be less than 100 characters.");
                }
            }
            if (!addressRg.IsMatch(tbAddress.Text) || tbAddress.Text.Length > 100) {
                errorsList.Add("A valid address must be entered.");
            }
            if (!textRg.IsMatch(tbCity.Text)) {
                errorsList.Add("City name must be 1 to 50 characters and contain only letters.");
            }
            if (!postalCodeRg.IsMatch(tbPostalCode.Text)) {
                errorsList.Add("Postal code must be formatted as A1A1A1 or A1A 1A1.");
            }
            if (cbProvince.SelectedIndex == -1) {
                errorsList.Add("A province must be selected.");
            }
            if (cbTitle.SelectedIndex == -1) {
                errorsList.Add("A title must be selected.");
            }
            if (!passwordRg.IsMatch(tbPassword.Password)) {
                errorsList.Add("Password must be at least 8 characters and contain one letter and one number.");
            }
            if (tbPassword.Password != tbConfirmPassword.Password) {
                errorsList.Add("Password must be correctly confirmed.");
            }
            if (errorsList.Count != 0) {
                MessageBox.Show(string.Join("\n", errorsList), "Registration Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return (errorsList.Count == 0);
        }

        private void ClearFields() {
            tbFName.Text = "";
            tbLName.Text = "";
            dpDoB.SelectedDate = null;
            cbGender.SelectedIndex = -1;
            tbMainPhone.Text = "";
            tbSecondaryPhone.Text = "";
            tbEmail.Text = "";
            tbAddress.Text = "";
            tbCity.Text = "";
            tbPostalCode.Text = "";
            cbProvince.SelectedIndex = -1;
            cbTitle.SelectedIndex = -1;
            tbPassword.Password = "";
            tbConfirmPassword.Password = "";
        }

        private string GenerateUsername() {
            string fname = tbFName.Text.Substring(0, 2).ToLower();
            string lname = tbLName.Text.Substring(0, 2).ToLower();

            // Generate a random number to avoid duplicate username
            Random random = new Random();
            int number = random.Next(1000, 9999);

            string username = lname + fname + number;

            return username;
        }
    }
}

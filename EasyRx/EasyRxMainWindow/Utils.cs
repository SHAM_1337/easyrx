﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace EasyRxMainWindow
{
    static class Utils
    {
        // used to process BLOB / byte array pictures
        public static BitmapImage ByteArrayToBitmapImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }

        public static BitmapImage GetBitmapImageV2(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        //Password hashing and salting tools
        public static string GenerateSalt() {
            var bytes = new byte[24];

            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            provider.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

        public static string GenerateHash(string password, string salt) {
            byte[] bytes = Encoding.UTF8.GetBytes(password + salt);
            SHA256Managed SHA256ManagedString = new SHA256Managed();
            byte[] hash = SHA256ManagedString.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }

        public static bool CompareHash(string plainTextInput, string hashedInput, string salt) {
            string HashedPassword = GenerateHash(plainTextInput, salt);
            return HashedPassword.Equals(hashedInput);
        }


    }
}

USE [EasyRx]
GO

INSERT INTO [dbo].[Drug]
           ([Name]
           ,[Provider]
           ,[Cost]
           ,[Price]
           ,[IsGeneric])
VALUES
('Lytenol', 'FakeCo', '5.99', '10', '0'),
('Madvil', 'BestRx', '4.99', '9', '0'),
('Choomba', 'LeaderX', '3.99', '8', '0'),
('Fylgud', 'CheapDrug', '2.99', '6', '1'),
('PainGone', 'MoarX', '1.99', '5', '1')
;

GO

INSERT INTO [dbo].[InsurancePlan]
           ([InsuranceCompany]
           ,[Coverage]
           ,[Address]
           ,[City]
           ,[PostalCode]
           ,[Province]
           ,[PhoneNumber]
           ,[FaxNumber])
VALUES
('CoverMaxGold', '9999.99', '123 Aitken Basin', 'NotAPlace', 'J1R 9K4', 'QC', '(514)123-4567', '(450)765-4321'),
('MediumPlanSilver', '4999.99', '666 Fake St', 'CityLand', 'K5C 9I8', 'BC', '(450)232-2233', '(444)333-2222'),
('BasicBronze', '1999.99', '678 Real Crescent', 'YouKnowWhere', '1A2 3B4', 'ON', '(111)222-3333', '(654)999-8080')
;
GO

INSERT INTO [dbo].[Clinic]
           ([Name]
           ,[PhoneNumber]
           ,[FaxNumber]
           ,[Email]
           ,[Address]
           ,[City]
           ,[PostalCode]
           ,[Province])
VALUES
('Bean-Toutu', '(555)111-2222', '(444)222-1111', 'info@beantoutu.ca', '222 Bean St', 'CityLand', '7U6 8I9', 'BC'),
('Marmaprix', '(444)444-4445', '(111)333-4445', 'info@marmaprix.ca', '333 Prix St', 'Night City', 'R5T Y6U', 'QC'),
('FamilyPharma', '(333)555-5555', '(445)654-3545', 'info@familypharma.ca', '14 Street St', 'ThatPlaceCity', 'W1E E4R', 'ON')
;
GO

SELECT * FROM [dbo].[Drug]
;
GO

SELECT * FROM [dbo].[Clinic]
;
GO

SELECT * FROM [dbo].[InsurancePlan]
;
GO

SELECT * FROM [dbo].[Customer]
;
GO
